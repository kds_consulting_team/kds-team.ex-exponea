import logging
import shutil
from tempfile import NamedTemporaryFile
from typing import Optional, List

from keboola.http_client import HttpClient


class ClientError(Exception):
    def __init__(self, message, reason, **kwargs):
        super().__init__(message)
        self.reason = reason


class ExponeaApiClient(HttpClient):

    def __init__(self, base_url: str, project_token: str, key_id: str, key_secret: str):
        if not base_url.endswith('/'):
            base_url += '/'
        base_url = f'{base_url}data/v2/projects/{project_token}/'
        super().__init__(base_url=base_url, auth=(key_id, key_secret))

    def get_customers(self, result_path: str, segment_id: Optional[str] = None):
        parameters = {"format": "csv"}

        resp = self.post_raw(endpoint_path='customers/export', json=parameters)
        self._handle_error(resp)
        if resp.encoding is None:
            resp.encoding = 'utf-8'

        with NamedTemporaryFile('wb', delete=False) as out_table:
            tmp_path = out_table.name
            out_table.write(resp.content)
        header = self._write_from_temp_to_table(tmp_path, result_path)
        # os.remove(tmp_path)
        return header

    @staticmethod
    def _write_from_temp_to_table(temp_file_path, table_path) -> List[str]:
        with open(temp_file_path, mode='r', encoding='utf-8') as in_file:
            first_line = in_file.readline()
            # shutil.copyfileobj(in_file, out_file)
        shutil.move(temp_file_path, table_path)
        return [h.strip() for h in first_line.split(',')]

    def get_events(self, customer_registered_id: str, customer_id_cookie: str, event_types: List[str],
                   limit: Optional[int] = None,
                   skip: Optional[int] = None):
        parameters = {"customer_ids": {"registered": customer_registered_id,
                                       "cookie": customer_id_cookie},
                      "event_types": event_types}

        resp = self.post_raw(endpoint_path='customers/events', json=parameters)
        self._handle_error(resp)
        return resp.json()

    def _handle_error(self, response):
        logging.debug(response)
        if response.status_code == 401:
            raise ClientError(
                f'Failed to login, verify your Key ID and Key Secret!. Details: '
                f'{response.json().get("errors", {}).get("description", "")}',
                reason={'full_message': response})
        elif response.status_code == 403:
            raise ClientError(
                f'Failed to retrieve the resource, check whether the token has appropriate Get permissions!. Details: '
                f'{response.json().get("errors", {}).get("description", "")}',
                reason={'full_message': response})
        elif response.status_code >= 300:
            raise ClientError(f'Request failed! Details: {response.status_code}:'
                              f'{response.json()}',
                              reason={'full_message': response.content})
