'''
Template Component main class.

'''
import csv
import logging

import keboola.utils.helpers as kbc_helpers
from keboola.component.base import ComponentBase
from keboola.component.exceptions import UserException

# configuration variables
from exponea_api.client import ExponeaApiClient, ClientError

KEY_KEY_SECRET = '#api_secret'
KEY_KEY_ID = 'key_id'
KEY_PROJECT_TOKEN = 'project_token'
KEY_ENDPOINTS = 'endpoints'
KEY_INCREMENTAL_OUTPUT = 'incremental_output'
KEY_LOADING_OPTIONS = 'loading_options'

KEY_CUSTOMERS = ''

# list of mandatory parameters => if some is missing,
# component will fail with readable message on initialization.
REQUIRED_PARAMETERS = [KEY_KEY_ID, KEY_KEY_SECRET, KEY_PROJECT_TOKEN, KEY_ENDPOINTS, KEY_LOADING_OPTIONS]
REQUIRED_IMAGE_PARS = []


class Component(ComponentBase):
    """
        Extends base class for general Python components. Initializes the CommonInterface
        and performs configuration validation.

        For easier debugging the data folder is picked up by default from `../data` path,
        relative to working directory.

        If `debug` parameter is present in the `config.json`, the default logger is set to verbose DEBUG mode.
    """

    def __init__(self):
        super().__init__()
        self.validate_configuration_parameters(REQUIRED_PARAMETERS)
        self.incremental_loading = self.configuration.parameters[KEY_LOADING_OPTIONS].get(KEY_INCREMENTAL_OUTPUT,
                                                                                          False)
        self._client = ExponeaApiClient(base_url=self.configuration.parameters['base_url'],
                                        project_token=self.configuration.parameters[KEY_PROJECT_TOKEN],
                                        key_id=self.configuration.parameters[KEY_KEY_ID],
                                        key_secret=self.configuration.parameters[KEY_KEY_SECRET]
                                        )

    def run(self):
        '''
        Main execution code
        '''

        # check for missing configuration parameters
        self.validate_configuration_parameters(REQUIRED_PARAMETERS)
        params = self.configuration.parameters
        endpoints = params[KEY_ENDPOINTS]
        if endpoints.get('customers') or (
                endpoints.get('events') and not endpoints.get('events').get('use_input_table')):
            logging.info('Downloading customers')
            customers_out = self.create_out_table_definition('customers.csv', primary_key=['customer_id_registered',
                                                                                           'customer_id_cookie'],
                                                             incremental=self.incremental_loading)

            # customers_out = self.create_out_file_definition('customers.csv')

            customer_columns = self._client.get_customers(customers_out.full_path)
            normalized_header = self._dedupe_header(customer_columns)
            customers_out.columns = normalized_header
            self.write_manifest(customers_out)

        if endpoints.get('events'):
            logging.info("Downloading events..")
            events_config = endpoints['events'][0]
            if events_config.get('use_input_table'):
                input_table = self.get_input_tables_definitions()[0]
            else:
                input_table = customers_out

            result_table = self.download_events(input_table, events_config)
            self.write_manifest(result_table)

    @staticmethod
    def _dedupe_header(header: list, index_separator='_'):
        new_header = list()
        dup_cols = dict()
        for c in header:
            if c.lower() in [h.lower() for h in new_header]:
                new_index = dup_cols.get(c, 0) + 1
                new_header.append(c + index_separator + str(new_index))
                dup_cols[c.lower()] = new_index
            else:
                new_header.append(c)
        return new_header

    def download_events(self, input_table, events_config: dict):
        output_table = self.create_out_table_definition('events.csv', primary_key=['type', 'timestamp'],
                                                        incremental=self.incremental_loading)
        with open(input_table.full_path, 'r', encoding='utf-8') as input_file, \
                open(output_table.full_path, 'w+', encoding='utf-8') as output_file:
            reader = csv.DictReader(input_file, fieldnames=input_table.columns)
            writer = csv.DictWriter(output_file, fieldnames=['type', 'timestamp', 'properties'])
            writer.writeheader()
            for row in reader:
                if events_config.get('types_from_input'):
                    types = kbc_helpers.comma_separated_values_to_list(row['event_types'])
                else:
                    types = kbc_helpers.comma_separated_values_to_list(events_config['types'])
                result = self._client.get_events(row['customer_id_registered'], row['customer_id_cookie'], types)
                self._write_events(result['data'], writer)
        return output_table

    def _write_events(self, data: dict, writer: csv.DictWriter):
        writer.writerows(data)


"""
        Main entrypoint
"""
if __name__ == "__main__":
    try:
        comp = Component()
        # this triggers the run method by default and is controlled by the configuration.action parameter
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except ClientError as exc:
        logging.error(exc, extra={"full_message": exc.reason})
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
